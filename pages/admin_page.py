from selenium.webdriver.common.by import By


class AdminPage:
    def __init__(self, driver):
        self.driver = driver

    def navigate_to_add_project(self):
        self.driver.find_element(By.CSS_SELECTOR, 'a[title="Administracja"]').click()
        self.driver.find_element(By.CSS_SELECTOR, 'a.button_link[href*="add_project"]').click()
