from selenium.webdriver.common.by import By


class ProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def create_project(self, name, prefix, description):
        self.browser.find_element(By.CSS_SELECTOR, 'input#name').send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, 'input#prefix').send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, 'textarea#description').send_keys(description)
        self.browser.find_element(By.CSS_SELECTOR, 'input#save').click()

    def search_project(self, name):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_puzzle_alt.icon-20').click()
        self.browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def get_first_project_title(self):
        title_in_search = (By.CSS_SELECTOR, '.article_in_content tbody tr td a')
        title_element = self.browser.find_element(*title_in_search)
        return title_element.text
