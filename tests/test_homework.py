from pages.admin_page import AdminPage
from pages.project_page import ProjectPage
from utils.random_generators import generate_random_string, generate_random_number
from fixtures.login import browser
from fixtures.chrome import chrome_browser


def test_e2e_project_creation(browser):
    admin_page = AdminPage(browser)
    project_page = ProjectPage(browser)

    random_text = generate_random_string(3)
    random_number = generate_random_number()

    project_name = f"MY PROJECT NUMBER {random_number}"
    project_prefix = f"WEB{random_text}".upper()
    project_description = f"Description of project {random_number}"

    admin_page.navigate_to_add_project()
    project_page.create_project(project_name, project_prefix, project_description)
    project_page.search_project(project_name)

    title_text = project_page.get_first_project_title()
    assert title_text == project_name
