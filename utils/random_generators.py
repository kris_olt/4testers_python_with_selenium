import random
import string


def generate_random_string(length):
    characters = string.ascii_letters
    return ''.join(random.choice(characters) for _ in range(length))


def generate_random_number():
    return random.randint(150000, 9999999)
